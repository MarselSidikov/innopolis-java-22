package anon;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        TextBox withoutSpacesTextBox = new TextBox() {
            @Override
            public String beforeEnterText(String text) {
                return text.replace(" ", "");
            }

            @Override
            public void afterEnterText() {
                System.out.println("Вы ввели - " + super.text);
            }
        };
        // анонимный класс, который описывает anon.TextBox, умеющий сохранять введенные тексты
        TextBox withMemoryTextBox = new TextBox() {
            // константа, говорит, сколько максимум текстов можно запомнить
            private static final int MAX_TEXTS_COUNT = 5;
            // массив всех текстов (сразу выделяем для него память в количестве 5 элементов)
            private String texts[] = new String[MAX_TEXTS_COUNT];

            // сколько уже запомнили текстов
            private int count = 0;

            @Override
            public String beforeEnterText(String text) {
                // метод запоминает текст, который ввел пользователь
                // сохраняет его в массив texts под индексом count
                if (count < MAX_TEXTS_COUNT) {
                    // под индексом count запоминает текст
                    texts[count] = text;
                    // count увеличиваем, чтобы на эту позицию встал следующий текст
                    count++;
                } else {
                    System.err.println("Слишком много слов. Память забита и будет очищена");
                    count = 0;
                }

                return text;
            }

            @Override
            public void afterEnterText() {
                System.out.println("Тексты, которые вы уже вводили:");
                // печатаем все тексты, которые ввел пользователь
                for (int i = 0; i < count; i++) {
                    System.out.println(texts[i]);
                }
            }
        };

        Scanner scanner = new Scanner(System.in);
        while (true) {
            String text = scanner.nextLine();
            withMemoryTextBox.enterText(text);
        }
    }
}