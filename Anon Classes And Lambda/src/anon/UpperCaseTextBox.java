package anon;

public class UpperCaseTextBox extends TextBox {
    @Override
    public String beforeEnterText(String text) {
        // все буквы делаем заглавными
        return text.toUpperCase();
    }

    @Override
    public void afterEnterText() {
        // просто выводим введенный пользователем текст
        System.out.println("Спасибо, вы ввели текст " + super.text);
    }
}
