import java.util.Random;

public class Main3 {

    // true, 50 -> все четные числа от 0 до 48 включительно
    // false, 50 -> все нечетные числа от 1 до 49
    public static int generateNumber(boolean isEven, int bound) {
        Random random = new Random();
        int number;
        while (true) {
            number = random.nextInt(bound);
            // если запросили четное и сгенерировали четное
            // или
            // если запросили нечетное и сгенерировали нечетное
            if ( (isEven && number % 2 == 0) || (!isEven && number % 2 == 1)) {
                // останавливаем цикл, функцию и возвращаем результат
                break;
            }
        }

        return number;
    }

    public static void main(String[] args) {
        System.out.println(generateNumber(false, 1));
    }
}
