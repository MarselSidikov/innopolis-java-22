package example05;

public class Container<E> {
    private static final int DEFAULT_SIZE = 5;

    private E[] elements;
    private int count;

    public Container() {
        this.elements = (E[])new Object[DEFAULT_SIZE];
        this.count = 0;
    }

    public void add(E element) {
        this.elements[count] = element;
        count++;
    }

    public E get(int index) {
        return this.elements[index];
    }
}
