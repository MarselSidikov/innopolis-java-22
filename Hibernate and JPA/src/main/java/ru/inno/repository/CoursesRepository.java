package ru.inno.repository;

import ru.inno.models.Course;

public interface CoursesRepository extends CrudRepository<Course> {
}
