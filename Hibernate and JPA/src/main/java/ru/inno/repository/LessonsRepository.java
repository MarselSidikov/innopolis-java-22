package ru.inno.repository;

import ru.inno.models.Lesson;

public interface LessonsRepository extends CrudRepository<Lesson> {
}
