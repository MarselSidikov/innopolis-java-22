package ru.inno.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import ru.inno.models.Lesson;

import java.util.List;

@RequiredArgsConstructor
public class LessonsRepositoryImpl implements LessonsRepository {

    private final EntityManager entityManager;

    @Override
    public void save(Lesson entity) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(entity);
        transaction.commit();
    }

    @Override
    public List<Lesson> findAll() {
        return null;
    }

    @Override
    public Lesson findById(Long id) {
        return null;
    }
}
