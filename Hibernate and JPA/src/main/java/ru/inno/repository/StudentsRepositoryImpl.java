package ru.inno.repository;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import ru.inno.models.Course;
import ru.inno.models.Student;

import java.util.List;

@RequiredArgsConstructor
public class StudentsRepositoryImpl implements StudentsRepository {

    private final EntityManager entityManager;

    @Override
    public void save(Student entity) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(entity);
        transaction.commit();
    }

    @Override
    public List<Student> findAll() {
        return null;
    }

    @Override
    public Student findById(Long id) {
        return entityManager.find(Student.class, id);
    }
}
