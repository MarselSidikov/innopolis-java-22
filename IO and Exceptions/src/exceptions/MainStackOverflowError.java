package exceptions;

public class MainStackOverflowError {

    public static void f() {
        f();
    }

    public static void main(String[] args) {
        try {
            f();
        } catch (StackOverflowError e) {
            System.out.println("Переполнение стека!");
        }
    }
}
