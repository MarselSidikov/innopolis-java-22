package table;

public class Table {
    // константа - максимальный размер таблицы
    private static final int MAX_TABLE_SIZE = 10;

    int count; // сколько всего пар есть в таблице

    Pair[] pairs; // какие пары у нас есть

    // конструктор
    public Table() {
        // создаем массив для 10 пар
        this.pairs = new Pair[MAX_TABLE_SIZE];
        // изначально пар у нас с вами нет
        this.count = 0;
    }

    // метод, который добавляет новую пару
    public void add(String key, int value) {
        // проверим, чтобы еще было место в нашем массиве
        if (count < MAX_TABLE_SIZE) {
            // надо проверить, а нет ли такого ключа уже в массиве
            for (int i = 0; i < count; i++) {
                // берем текущую пару
                Pair current = pairs[i];
                // а не содержит ли эта пара такой же ключ
                if (key.equals(current.key)) {
                    // заменяем старое значение новым
                    current.value = value;
                    // останавливаем выполнение этого метода
                    return;
                }
            }
            // если место есть - создаем новую пару
            Pair pair = new Pair(key, value);
            pairs[count] = pair;
            count++;
        } else {
            System.err.println("Массив переполнен");
        }
    }

    // метод для получения значения по ключу
    public int get(String key) {
        for (int i = 0; i < count; i++) {
            // берем текущую пару из массива
            Pair current = pairs[i];

            // смотрим, совпадает ли ключ, который лежит в этой паре, с ключом, который я запросил
            if (key.equals(current.key)) {
                // возвращаю значение
                return current.value;
            }
        }
        // если ничего не нашли
        System.err.println("Ничего нет!");
        return -1;
    }

    // вложенный класс
    static class Pair {
        // ключ
        String key;
        // значение
        int value;

        public Pair(String key, int value) {
            this.key = key;
            this.value = value;
        }
    }

    // внутренний класс
    public class Printer {
        private String separator;

        public Printer(String separator) {
            this.separator = separator;
        }

        public void print() {
            for (int i = 0; i < count; i++) {
                System.out.println(pairs[i].key
                        + separator + pairs[i].value);
            }
        }
    }
}
