package ru.inno.util.app;

import ru.inno.util.config.Config;

import java.util.List;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.Parameters;

@Parameters(separators = "=")
public class Program {

	@Parameter(names = {"--files"})
	private List<String> files;

	public static void main(String[] args) {
		Config config = new Config();
		Program program = new Program();

		JCommander.newBuilder()
			.addObject(program)
			.build()
			.parse(args);

		System.out.println(program.files);
		System.out.println(config.getFileExtension());
	}
}