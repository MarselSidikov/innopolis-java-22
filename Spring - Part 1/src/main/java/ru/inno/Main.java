package ru.inno;

import com.zaxxer.hikari.HikariDataSource;
import ru.inno.repositories.impl.UsersRepositoryFilesImpl;
import ru.inno.repositories.impl.UsersRepositoryJdbcImpl;
import ru.inno.services.UsersServiceImpl;
import ru.inno.validators.impl.EmailRegexValidator;
import ru.inno.validators.impl.EmailSimpleValidator;
import ru.inno.validators.impl.PasswordLengthValidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

public class Main {
    public static void main(String[] args) {

        Properties dbProperties = new Properties();

        try {
            dbProperties.load(new BufferedReader(
                    new InputStreamReader(Main.class.getResourceAsStream("/application.properties"))));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariDataSource dataSource = new HikariDataSource();
        dataSource.setPassword(dbProperties.getProperty("db.password"));
        dataSource.setUsername(dbProperties.getProperty("db.username"));
        dataSource.setJdbcUrl(dbProperties.getProperty("db.url"));
        dataSource.setMaximumPoolSize(Integer.parseInt(dbProperties.getProperty("db.hikari.maxPoolSize")));

        String regex = "^(?=.{1,64}@)[A-Za-z0-9_-]+(\\.[A-Za-z0-9_-]+)*@[^-][A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";

        EmailRegexValidator emailRegexValidator = new EmailRegexValidator(regex);
        EmailSimpleValidator emailSimpleValidator = new EmailSimpleValidator("@.");

        UsersRepositoryFilesImpl usersRepositoryFiles = new UsersRepositoryFilesImpl("users.txt");
        UsersRepositoryJdbcImpl usersRepositoryJdbc = new UsersRepositoryJdbcImpl(dataSource);

        PasswordLengthValidator passwordLengthValidator = new PasswordLengthValidator();
        passwordLengthValidator.setMinLength(7);

        UsersServiceImpl usersService = new UsersServiceImpl(usersRepositoryJdbc, emailRegexValidator, passwordLengthValidator);
        usersService.signUp("aqua@gmail.com", "qwerty010");
    }
}