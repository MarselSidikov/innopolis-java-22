package ru.inno.repositories.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.inno.models.User;
import ru.inno.repositories.UsersRepository;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

@Component
public class UsersRepositoryFilesImpl implements UsersRepository {

    @Value("${files.path}")
    private String fileName;

    @Override
    public void save(User user) {
        try (FileWriter fileWriter = new FileWriter(fileName, true);
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter)) {
            String userToSave = user.getEmail() + "|" + user.getPassword();
            bufferedWriter.write(userToSave);
            bufferedWriter.newLine();
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }
    }
}
