package ru.inno.services;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import ru.inno.models.User;
import ru.inno.repositories.UsersRepository;
import ru.inno.validators.EmailValidator;
import ru.inno.validators.PasswordValidator;

@Component
public class UsersServiceImpl {

    private final UsersRepository usersRepository;
    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;

    @Autowired
    public UsersServiceImpl(@Qualifier("usersRepositoryJdbcImpl") UsersRepository usersRepository,
                            @Qualifier("emailRegexValidator") EmailValidator emailValidator,
                            PasswordValidator passwordValidator) {
        this.usersRepository = usersRepository;
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }

    public void signUp(String email, String password) {
        emailValidator.validate(email);
        passwordValidator.validate(password);

        User user = User.builder()
                .email(email)
                .password(password)
                .build();

        usersRepository.save(user);
    }
}
