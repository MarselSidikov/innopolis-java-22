package ru.inno.repositories;

import ru.inno.models.User;

import java.util.List;

public interface UsersRepository {
    List<User> findAll();
}
